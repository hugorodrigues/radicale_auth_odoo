import string
import random
import xmlrpc
import logging
import unittest
from xmlrpc.client import ServerProxy

import radicale_auth_odoo


TEST_VALID_GROUP = "base.group_erp_manager"
TEST_INVALID_GROUP = "base.group_portal"


def random_str(k=16, alpha=True):
    chars = string.ascii_letters
    if alpha:
        chars += string.digits

    return ''.join(random.choice(chars) for _ in range(k))


class MockConfig(object):
    """Mock configuration"""
    def __init__(self, demo_url="https://demo.odoo.com/start", group=False):
        self.group = group
        with xmlrpc.client.ServerProxy(demo_url) as proxy:
            login_data = proxy.start()

        self.valid_user = login_data["user"]
        self.valid_passwd = login_data["password"]
        self.host = login_data["host"]
        self.database = login_data["database"]

    def _get_config(self):
        config = {
            "auth": {
                "odoo_url": self.host,
                "odoo_database": self.database
                }
            }
        if self.group:
            config["auth"]["odoo_group"] = self.group
        return config

    def get(self, a, b):
        """
        Get config
        """
        return self._get_config()[a][b]

    def has_option(self, a, b):
        """
        Check if config is avaialable
        """
        try:
            self.get(a, b)
            return True
        except KeyError:
            return False


class Authentication(unittest.TestCase):
    """
    Tests Odoo logins
    """
    configuration = None
    logger = None
    valid_user = None
    valid_passwd = None
    invalid_passwd = None

    @classmethod
    def setUpClass(cls):
        cls.configuration = MockConfig()
        cls.logger = logging.getLogger(__name__)
        cls.valid_user = cls.configuration.valid_user
        cls.valid_passwd = cls.configuration.valid_passwd
        while True:
            random_pwd = random_str()
            if random_pwd != cls.valid_passwd:
                cls.invalid_passwd = random_pwd
                break

    def do_test(self, passwd):
        auth = radicale_auth_odoo.Auth(self.configuration,
                                       self.logger)
        return auth.is_authenticated(self.valid_user, passwd)

    def test_valid_login(self):
        self.configuration.group = False
        self.assertTrue(self.do_test(self.valid_passwd))

    def test_invalid_login(self):
        self.configuration.group = False
        self.assertFalse(self.do_test(self.invalid_passwd))

    def test_valid_login_with_valid_group(self):
        self.configuration.group = TEST_VALID_GROUP
        self.assertTrue(self.do_test(self.valid_passwd))

    def test_invalid_login_with_valid_group(self):
        self.configuration.group = TEST_VALID_GROUP
        self.assertFalse(self.do_test(self.invalid_passwd))

    def test_valid_login_with_invalid_group(self):
        self.configuration.group = TEST_INVALID_GROUP
        self.assertFalse(self.do_test(self.valid_passwd))

    def test_invalid_login_with_invalid_group(self):
        self.configuration.group = TEST_INVALID_GROUP
        self.assertFalse(self.do_test(self.invalid_passwd))

if __name__ == "__main__":
    unittest.main()

